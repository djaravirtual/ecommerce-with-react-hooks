import { useReducer } from 'react';
import connectionAxios from '../apiConnection';

const initialState = {
  loading: false,
  favorites: [],
  characters: [],
};

const favoriteReduder = (state, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        ...state,
        loading: action.payload,
      };
    case 'SET_CHARACTERS':
      return {
        ...state,
        characters: action.payload,
      };
    case 'ADD_TO_FAVORITE':
      return {
        ...state,
        favorites: [...state.favorites, action.payload],
      };
    default:
      return state;
  }
};

export const useCharacters = () => {
  const { apiConnection } = connectionAxios();
  const [dataResponse, dispatch] = useReducer(favoriteReduder, initialState);

  const onGetCharacters = async () => {
    try {
      dispatch({ type: 'SET_LOADING', payload: true });
      await apiConnection
        .get(`/character`)
        .then((response) => {
          const { status, data } = response;
          if (status === 200) {
            dispatch({ type: 'SET_CHARACTERS', payload: data.results });
          }
          dispatch({ type: 'SET_LOADING', payload: false });
        })
        .catch((error) => {
          console.error(error);
        });
    } catch (ex) {
      console.error('Error no controlado', ex);
    }
  };

  const onClickAddFavorite = (favorite) =>
    dispatch({ type: 'ADD_TO_FAVORITE', payload: favorite });

  return {
    onGetCharacters,
    onClickAddFavorite,
    dataResponse,
  };
};
