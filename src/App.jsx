import DataCharacters from './components/DataCharacters';
import Header from './components/Header';

function App() {
  return (
    <>
      <div className="px-6 bg-gray-200 h-screen flex flex-col dark:bg-gray-800 ">
        <Header />
        <main className=" flex-1">
          <DataCharacters />
        </main>
        <footer>Footer</footer>
      </div>
    </>
  );
}

export default App;
