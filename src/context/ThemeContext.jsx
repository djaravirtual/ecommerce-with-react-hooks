import { createContext, useState, useReducer } from 'react';
import { useCharacters } from '../hooks/useCharacters';

const ThemeContext = createContext();

export function ThemeContextProvider({ children }) {
  const { onGetCharacters, onClickAddFavorite, dataResponse } = useCharacters();
  const [darkMode, setDarkMode] = useState(false);

  const onClickDarkMode = () => {
    setDarkMode(!darkMode);
    if (darkMode) {
      document.documentElement.classList.remove('dark');
    } else {
      document.documentElement.classList.add('dark');
    }
  };

  return (
    <ThemeContext.Provider
      value={{
        darkMode,
        dataResponse,
        onClickDarkMode,
        onGetCharacters,
        onClickAddFavorite,
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
}

export default ThemeContext;
