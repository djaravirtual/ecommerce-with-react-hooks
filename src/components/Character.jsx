import React from 'react';
import { useContext } from 'react';
import ThemeContext from '../context/ThemeContext';

const Character = ({ item, addFavorite }) => {
  const { onClickAddFavorite } = useContext(ThemeContext);
  return (
    <div className="border rounded-lg overflow-hidden shadow-lg">
      <img src={item.image} alt="" className="" />
      <span className="py-2 text-xl font-semibold text-center block">
        {item.name}
      </span>
      <div className="p-3 text-gray-400 text-sm">
        <p>
          Status: <span>{item.status}</span>
        </p>
        <p>
          Gender: <span>{item.gender}</span>
        </p>
        <p>
          Species: <span>{item.species}</span>
        </p>
        <p>
          Location: <span>{item.location.name}</span>
        </p>
        {addFavorite && (
          <button
            type="button"
            onClick={() => onClickAddFavorite(item)}
            className=" block w-full mt-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Add Favorite
          </button>
        )}
      </div>
    </div>
  );
};

export default Character;
