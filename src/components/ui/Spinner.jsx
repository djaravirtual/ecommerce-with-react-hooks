import React from 'react';
import './Spinner.css';
const Spinner = () => {
  return (
    <div className="h-full flex items-center justify-center">
      <div className="lds-ripple">
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Spinner;
