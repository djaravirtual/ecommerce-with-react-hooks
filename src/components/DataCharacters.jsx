import React, { useContext, useEffect } from 'react';
import Spinner from './ui/Spinner';
import Character from './Character';
import ThemeContext from '../context/ThemeContext';

const DataCharacters = () => {
  const { onGetCharacters, dataResponse } = useContext(ThemeContext);

  useEffect(() => {
    onGetCharacters();
  }, []);

  return (
    <div className="h-full">
      {dataResponse.loading ? (
        <Spinner />
      ) : (
        <>
          <p className="py-4 text-2xl font-bold">List Favorites</p>
          <div className="grid grid-cols-5 gap-4">
            {dataResponse.favorites.map((favorite) => (
              <Character key={favorite.id} item={favorite} />
            ))}
          </div>

          <p className="py-4 text-2xl font-bold">List Characters</p>
          <div className="grid grid-cols-5 gap-4">
            {dataResponse.characters.map((character) => (
              <Character
                key={character.id}
                item={character}
                addFavorite={true}
              />
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default DataCharacters;
