import React, { useContext, useState } from 'react';
import reactLogo from '../assets/react.svg';
import ThemeContext from '../context/ThemeContext';

const Header = () => {
  const { darkMode, onClickDarkMode } = useContext(ThemeContext);

  return (
    <div className="h-24 p-4 bg-white flex items-center justify-between rounded-md shadow-md my-3">
      <img src={reactLogo} className="w-12 " alt="React logo" />

      <h2 className="text-2xl font-medium">React Hooks</h2>
      <button
        type="button"
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        onClick={onClickDarkMode}
      >
        {darkMode ? 'Dark Mode' : 'Light Mode'}
      </button>
    </div>
  );
};

export default Header;
